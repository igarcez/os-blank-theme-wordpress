<?php
/*
 * Funções extras úteis
 */

function getTrimTitle($chars = 20, $text = ' ...') {
	global $post;
	$title = get_the_title();
	if(strlen($title) <= $chars) {
		return $title;
	} else {
		$title = substr($title, 0, $chars) . $text;
		return $title;
	}
}

function getTrimContent($chars = 60, $text = ' ...') {
	global $post;
	$content = get_the_content();
	if(strlen($content) <= $chars) {
		return $content;
	} else {
		$content = substr($content, 0, $chars) . $text;
		return $content;
	}
}

function thumbnailOnBackground($size) {
	$size = array($size,$size);
	global $post;
	if ( has_post_thumbnail()) {
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $size );
		return $thumb['0'];
	} else {
		return 'sem imagem';
	}
}

function displayThumbnailOnBackground($size) {
	echo thumbnailOnBackground($size);
}

function getDados($dado, $id = 0) {
	if ($id == 0 )
		$id = get_the_ID();
	global $post;
	$opcoes = get_post_custom($id);

	return $opcoes[$dado][0];
}

function facebookLikeButton(){
	global $post;
	$url = get_permalink();
	$html = '<iframe src="//www.facebook.com/plugins/like.php?href=' . $url . '&amp;send=false&amp;layout=button_count&amp;width=100&amp;show_faces=false&amp;colorscheme=light&amp;action=like&amp;height=35&amp;appId=389828707753360" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:100px; height:35px;" allowTransparency="true"></iframe>';
	return $html;
}

function displayFacebookLikeButton() {
	echo facebookLikeButton();
}

function clear() {
	return '<div class="clear"></div>';
}
add_shortcode('clear', 'clear');
?>