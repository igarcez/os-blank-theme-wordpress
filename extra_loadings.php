<?php
/* load extra */
add_action( 'wp_enqueue_scripts', 'os_meus_scripts' );

function os_meus_scripts() {

	wp_register_script( 'custom-js', get_template_directory_uri() . '/js/custom.js', true );
	wp_enqueue_script('jquery');
	wp_enqueue_script('custom-js');

	// carrega o css do grid
	wp_register_style( 'grid', get_template_directory_uri() . '/css/grid/960.css');
    wp_enqueue_style( 'grid' );
}
?>