<?php
/* shortcodes referente ao grid system */

//função para pegar o alpha e omega
function get_alpha_omega($atts){
	if (!is_array($atts))
		return '';

	$class_to_return = '';
	foreach ($atts as $att) {
		if ($att == 'alpha')
			$class_to_return .= 'alpha ';
		if ($att == 'omega')
			$class_to_return .= 'omega ';
	}
	return $class_to_return;
}

function container_12($atts, $content = null) {
	return '<div class="container_12 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('container_12', 'container_12');

function container_16($atts, $content = null) {
	return '<div class="container_16 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('container_16', 'container_16');

function grid_1($atts, $content = null) {
	return '<div class="grid_1 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_1', 'grid_1');

function grid_2($atts, $content = null) {
	return '<div class="grid_2 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_2', 'grid_2');

function grid_3($atts, $content = null) {
	return '<div class="grid_3 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_3', 'grid_3');

function grid_4($atts, $content = null) {
	return '<div class="grid_4 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_4', 'grid_4');

function grid_5($atts, $content = null) {
	return '<div class="grid_5 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_5', 'grid_5');

function grid_6($atts, $content = null) {
	return '<div class="grid_6 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_6', 'grid_6');

function grid_7($atts, $content = null) {
	return '<div class="grid_7 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_7', 'grid_7');

function grid_8($atts, $content = null) {
	return '<div class="grid_8 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_8', 'grid_8');

function grid_9($atts, $content = null) {
	return '<div class="grid_9 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_9', 'grid_9');

function grid_10($atts, $content = null) {
	return '<div class="grid_10 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_10', 'grid_10');

function grid_11($atts, $content = null) {
	return '<div class="grid_11 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_11', 'grid_11');

function grid_12($atts, $content = null) {
	return '<div class="grid_12 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_12', 'grid_12');

function grid_13($atts, $content = null) {
	return '<div class="grid_13 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_13', 'grid_13');

function grid_14($atts, $content = null) {
	return '<div class="grid_14 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_14', 'grid_14');

function grid_15($atts, $content = null) {
	return '<div class="grid_15 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_15', 'grid_15');

function grid_16($atts, $content = null) {
	return '<div class="grid_16 ' . get_alpha_omega($atts) . '">' . do_shortcode($content) . '</div>';
}
add_shortcode('grid_16', 'grid_16');
?>